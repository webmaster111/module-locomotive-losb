# Locomotive LOSB

Шаблон HTML/JS/CSS для отображения структуры сайта

## Использование

1. Подключите файл стилей `locomotive-losb.css`, а также скрипт `locomotive-losb.js`
2. Наполните и адаптируйте шаблон `locomotive-losb.html` под конкретную структуру сайта

## Пример
![LOSB 1](https://gitlab.com/locomotive-dev/module-locomotive-losb/-/blob/master/images/locomotive-losb-1.png)
![LOSB 2](https://gitlab.com/locomotive-dev/module-locomotive-losb/-/blob/master/images/locomotive-losb-2.png)
![LOSB 3](https://gitlab.com/locomotive-dev/module-locomotive-losb/-/blob/master/images/locomotive-losb-3.png)

## License
[MIT](https://choosealicense.com/licenses/mit/)