function ready(e) {
            "loading" != document.readyState ? e() : document.addEventListener("DOMContentLoaded", e)
        }
        ready(
            () => {
                const e = e => {
                        event.preventDefault();
                            document.querySelectorAll(".losb-menu-element").forEach(el => {
                                el.classList.remove("active")
                            }); 
                        e.currentTarget.classList.add("active");
                  
                        let t = e.currentTarget.dataset.id;
                         t && c(t)
                    };
                    const t = e => {
                        e.preventDefault(), document.querySelectorAll(".losb-menu-element-sub").forEach(e => {
                                e.classList.remove("active")
                            }),
                            e.currentTarget.classList.add("active");
                        let t = e.currentTarget.dataset.id;
                        t && a(t)
                    };
                    const c = e => {
                        document.querySelectorAll(".losb-content").forEach(e => e.classList.remove("active")), console.log(e), document.querySelector(`.losb-content[data-id="losb-content-${e}"]`).classList.add("active")
                    };
                    const a = e => {
                        document.querySelectorAll(".losb-content-sub").forEach(e => e.classList.remove("active")), document.querySelector(`.losb-content-sub[data-id="losb-content-sub-${e}"]`).classList.add("active")
                    };
              
                    document.querySelectorAll(".losb-menu-element").forEach(t => {
                        t.addEventListener("click", e)
                    }),
                    document.querySelectorAll(".losb-menu-element-sub").forEach(e => {
                        e.addEventListener("click", t)
                    })
            }
        );
        
